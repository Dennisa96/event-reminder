package user;

import java.io.*;
import java.util.Scanner;

/**
 * User management; use a text file for storage
 */
public class UserStorage {

    public void save(User user) throws Exception {
        FileWriter fileWriter = new FileWriter("src/users.txt", true);
        fileWriter.append(user.getUserName());
        fileWriter.append(",");
        fileWriter.append(user.getPassword());
        fileWriter.close();
    }

    public User getByUsername(String userName) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File("src/users.txt"));

        while (scanner.hasNext()) {
            String line = scanner.nextLine();
            String[] elements = line.split(",");
            if(elements[0].equals(userName)) {
                User user = new User();
                user.setUserName(elements[0]);
                user.setPassword(elements[1]);
                return user;
            }
        }

        return null;
    }
}
